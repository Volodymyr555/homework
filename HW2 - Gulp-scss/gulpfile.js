const { src, dest, watch, series } = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const concat = require("gulp-concat");
const uglify = require("gulp-uglify-es").default;
const browserSync = require("browser-sync").create();
const imagemin = require("gulp-imagemin");
const autoprefixer = require("gulp-autoprefixer");
const del = require("gulp-clean");
const htmlmin = require("gulp-htmlmin");

function clean() {
  return src("dist/")
    .pipe(del());
};

function styles() {
  return src("src/styles/style.scss")
    .pipe(sass({ outputStyle: "compressed" }))
    .pipe(concat("style.min.css"))
    .pipe(dest("dist/css"))
    .pipe(browserSync.stream());
}

function img() {
    return src("src/img/*")
        .pipe(imagemin())
        .pipe(dest("dist/img"));
}

function scripts() {
  return src(["src/scripts/app.js"])
    .pipe(concat("app.min.js"))
    .pipe(uglify())
    .pipe(dest("dist/js"))
    .pipe(browserSync.stream());  
};

function html() {
  return src("*.html")
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(dest("dist"))
    .pipe(browserSync.stream())
};

function watching() {
  browserSync.init({
    server: {
      baseDir: "./"
    },
  });
  watch(["src/styles/style.scss"], styles);
  watch(["src/scripts/app.js"], scripts);
  watch(["*.html"]).on("change", browserSync.reload);
}



function autoPrefix() {
  return src("src/styles/style.scss")
  .pipe(
    autoprefixer({
      cascade: false,
    })
  )
  .pipe(dest("dist"))
};







exports.clean = clean;
exports.styles = styles;
exports.scripts = scripts;
exports.watching = watching;
exports.img = img;
exports.autoPrefix = autoPrefix;
exports.html = html;
exports.build = series(clean, styles, autoPrefix, scripts, img);
exports.dev = watching;
