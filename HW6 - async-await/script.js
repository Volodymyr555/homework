//Теоретичне питання. Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript.
//Відповідь: Завдання у Javascript виконуються восновному в однопотоковому режимі, тобто одне за одним.
//Асинхронність дозволяє виконувати окремі завдання паралельно та незалежно від інших завдань, отримуючи результат там де він потрібен.


const body = document.querySelector('body');
body.innerHTML = "<div><button>Знайти по IP</button></div><ul></ul>";
const btn = document.querySelector('button');
const ul = document.querySelector('ul');

btn.addEventListener('click', (event) => {
  
async function getIP() {
    const response = await fetch("https://api.ipify.org/?format=json");
  const data = await response.json();

  const ip = data.ip
  

  
 async function postData() {
  const response = await fetch(`http://ip-api.com/json/${ip}`);
   const data = await response.json();  

   const user = {
     континент: data.timezone,
     країна: data.country,
     регіон: data.regionName,
     місто: data.city  

   }
   ul.insertAdjacentHTML(
     "beforeend",
     `<li>${user.континент}</li>
      <li>${user.країна}</li>
      <li>${user.регіон}</li>
      <li>${user.місто}</li>`
   );

  }  
  postData()

}

getIP()
 })












  