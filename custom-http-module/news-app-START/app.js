// Custom Http Module
function customHttp() {
  return {
    get(url, cb) {
      try {
        const xhr = new XMLHttpRequest();
        xhr.open('GET', url);
        xhr.addEventListener('load', () => {
          if (Math.floor(xhr.status / 100) !== 2) {
            cb(`Error. Status code: ${xhr.status}`, xhr);
            return;
          }
          const response = JSON.parse(xhr.responseText);
          cb(null, response);
        });

        xhr.addEventListener('error', () => {
          cb(`Error. Status code: ${xhr.status}`, xhr);
        });

        xhr.send();
      } catch (error) {
        cb(error);
      }
    },
    post(url, body, headers, cb) {
      try {
        const xhr = new XMLHttpRequest();
        xhr.open('POST', url);
        xhr.addEventListener('load', () => {
          if (Math.floor(xhr.status / 100) !== 2) {
            cb(`Error. Status code: ${xhr.status}`, xhr);
            return;
          }
          const response = JSON.parse(xhr.responseText);
          cb(null, response);
        });

        xhr.addEventListener('error', () => {
          cb(`Error. Status code: ${xhr.status}`, xhr);
        });

        if (headers) {
          Object.entries(headers).forEach(([key, value]) => {
            xhr.setRequestHeader(key, value);
          });
        }

        xhr.send(JSON.stringify(body));
      } catch (error) {
        cb(error);
      }
    },
  };
}
// init http module

const http = customHttp();
const newsService = (function () {
    const API_KEY = `42a7ce1d3b8e4dab8a583dbdcf0d30c2`;
    const API_URL = `https://newsapi.org/v2`;
    return {
        topHeadLines(country = `ua`, callback) {
            http.get(`${API_URL}/top-headlines?country=${country}&apiKey=${API_KEY}`, callback)
        },
        everything(query, calback) {
            http.get(`${API_URL}/everything?q=${query}&apiKey=${API_KEY}`, callback)
        
        }
    }

    }) ();

//init selects

document.addEventListener('DOMContentLoaded', () => {
    M.AutoInit();
    loadNews();

});

// on load news from BE
function loadNews() {
    newsService.topHeadLines('ua', onGetRasponse)
}

function onGetRasponse(err, response) {
    if (err) {
      throw new Error(err);       
    }
        

const isLoad = isStatusOk(response.status);
if (!isLoad) {
  throw new Error(err);
}
renderNews(response.articles);
}

function renderNews(news) {
  const newsContainer = document.querySelector('.news-container .row');
  let fragment = '';
  if (!news || !news.length) {
    fragment += 'No news! :)';
    
  }
  news.forEach((article) => {
    const newsItem = createNewsTemplate(article);
    fragment += newsItem;
    newsContainer.insertAdjacentElement('afterbegin', fragment)
  })
  
}
function createNewsTemplate({ urlToImage, url, title = "", description = "" }) {
  const URL_IMAGE_DEFAULT =
    "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8NDQ0NDQ0NDQ0NDQ0ODQgIDQ8IDQ4NFREWFhYRExMYHSgsGBolGxMTITEhJSkrLi4uFx8zODMsNygtLjcBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAaAAEBAQEBAQEAAAAAAAAAAAAAAQIEAwUH/8QANRABAQABAQUDCQgCAwAAAAAAAAERAgMEITFREhRBBTJSYWKBkZKhEyJCcYKxweFy0SMz8P/EABQBAQAAAAAAAAAAAAAAAAAAAAD/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwD9EAACKCCgIKoMjWAGRswDA3gwDA3gwDA3gwDA3hAZGsICCgIKlAAAAAAAipFABQAUAFBFXBgEMLhQZwYaAZGsJgGRcGAZFARFARKqUAAAAAACKRQAUBRcAi4VYCC4XAINGAZwYawYBnCYbwgMo0AxRpKDKNVASpVqUEAAAAABVSKCgsgLFIsAXBI1ICSLhcLgEMNYXAMYMN4MAwmG8JgGMJXpYzYDCNWJQZwjSYBipWqzQQAAAAAFjUZjUBY0kagEaiSNASNSEbkBJFkXDUgMyLh67PZatXKW+ucnTs9x9K+7T/sHDh7bPdNerwxOurg+jp2ejZzOJPa1c/ixd705kmbm4zygPn7xsPs7jOeEsvJ44fU3/Z5058dN+j51gPLCYetjFgPOxK3WbAYRqoDzrNbsYoIAAAAADUajEb0g0sSNQGpGmY6Nju2vXy08PSvCA89LemZuJLfVOLu2Xk+Tz7n2dPCOrTp0aMSdnTnhJytBwbLctV5/dnr434OvZ7pp08/vX2uXwe9/94vDXsLq567jpJiA1r2+nTwzn2dPFz7TetV5THr516dznpX4HdJ6V+AOTVm87b67xTDs7pPSvwO6T0r8Aeuyvb0cfGYsfN16MWy85wfS2Oy7GZnOevBjbbtNVznHqkyD5tjFj6PcZ6V+Dm3vd5s8cbc558ActjNbrNB51G7GKDFedempigyAAAAACx6aXnpekBqLEiwG4+n5L2mZdHTjPyr5jo3TadjXpvhyv5UHZv2116dWJcSznOfxeG7+fpzzzzrr37Z50Z8dPH3eLk3bz9H5g7t613TplnDjJ1c03nX1+kdG/ebP8p+1c2w0drVjw50Gu8a+v0i941dfpHTdhpxy985uLVMWzpQeneNXX6Q7xq6/SPNdGm6riA1d419fpEu8a+v0jzqA6d22+rVqxbmYvhIz5S/B+r+Gdz8/3Vryj+D9X8A4KzW6xQYrFbrFBnU869NTzoM0KAAAAAseml5xvSDcWMxqA3GoxK3KD7O66+3s5njw7NcWy09nazT01NeTdpjVdPpTM/OPfb6P+TZ6utxfzBrf/Mn+U/auXY6+zZfp6nTv/mT/ACn7VxSg77vWnHDOenJyXVm56ppltxONvg3tNldPOe+cYDLt3fZ9mced5vHdNln715Tl+bsBzbXdu1qzLiXn+bk2uns6rOni+o4d/wBONUvWY98Bnc/P91a8pfg/V/DG5ef7q35T/B+r+AcFZrVYoM1it1igxqY1N6mKDIAAAAALG9LEagNrGY1AbjUecrUB67PXdNmqc5ZX2+GqS+HDVHwpX1PJu0zo7Pjpv0Bryh5k/wAp+1cM6fs7vKEt0TEt+9OEmfCuLR29NzNOqXr2bQfQ3bYdmZvnX6ep7WZ/1Xzvttr7Xyf0v2+19r5f6B9DTMcJw9UV877fa+18v9H2+19r5f6B9F4b5pzovq4uT7ba+18n9Jq2u1sx97jw8z+gXcf+z3Vryp+D9X8M7jos2nHTZOzeNljXlW+Z+r+AcFrNLWbQSpSpQZ1MamqxQQAAAAAFjUZagLGpWGoDUalYUHpl0blt+xrzfNsxfFyStSg+z3/Z+lflq9+2fW/LXxpVyD7Hfdn1vy077s+t+Wvk5Mg+t33Z+lflp33Z9b8tfJyZB9bvuz635ad+2fW/LXyLUyD7Hftn1vy1xeUN406+z2bnHazmWdHJlm0FrNplmgIVKDNZrVZoIAAAAACqzGgURQayuWSA2sZMg3lZWFBvJlhcg1kyzkyDWUtZyZBUTKAqFTIFZAEqValBAAAAAAIqRQURQURQXK5ZUGlyxlQayZZXINGWMrkFymTKZBUymQC1AAQAEolAAAAAAAipFAABRAGhAFEUBUAUQAAAEAAQFQAEqpQAAAAAAAAUQyCiZMgomTINDOTINDOTINDOTINDOTINImTIKJkyCiZMgqUyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA//9k=";
  return `
    <article class="col s12">
    <div class="card">
      <div class="card-image">
          <img src="${urlToImage || URL_IMAGE_DEFAULT}" alt="news image"/>
          <h2>${title}</h2>
        </div>
        <div class="card-content">
          <p>${description}</p>
        </div>
        <div class="card-action">
          <a href="${url}" target="blank">Read more</a>
        </div>
  </div>
  </article >
  `;    
}

function isStatusOk(status) {
  return status === 'ok';
  
}