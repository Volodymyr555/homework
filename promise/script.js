// function http() {
//     return {
//         get(url, callback) {
//             try {
//                 const xhr = new XMLHttpRequest();
//                 xhr.open('GET', url);
//                 xhr.addEventListener('load', () => {
//                     console.log(xhr.status);

//                     const resp = JSON.parse(xhr.responseText);
//                     callback(null, resp)
//                 });
//                 xhr.send();
                
//             } catch (err) {
//                 callback(err)
                
//             }
//         }
//     }
    
// }

// function getUsers() {
//     return new Promise((resolve, reject) => {
//         http().get("https://jsonplaceholder.typicode.com/users", (err, response) => {
//             if (err) {
//               reject(err);
//             }
//             resolve(response);
//           }
//         );
//     })
    
// }

// function getPosts(id) {
//     return new Promise((resolve, reject) => {
//         http().get("https://jsonplaceholder.typicode.com/posts/${id}", (err, response) => {
//             if (err) {
//               reject(err);
//             }
//             resolve(response);
//           }
//         );
//     })
    
// }

// getUsers().then((data) => {
//     console.log(data)
//     return getPosts(data, id)
// });

fetch('https://jsonplaceholder.typicode.com.todos/1', {'GET'})
    .then((data) => data.JSON())
    .then(todo => {
        const body = document.querySelector('body');
        body.insertAdjacentHTML('beforeend', <b>${todo.title}</b>)
    })

