
const tabs = document.querySelector(".tabs");
const alltabsContent = document.querySelectorAll(".tabs-content-li");

let selectedTab;

tabs.onclick = function (event) {
  let li = event.target.closest("li");

  if (!li) return;

  if (!tabs.contains(li)) return;

  highlight(li);

  function highlight(li) {
    if (selectedTab) {
      selectedTab.classList.remove("active");
    }
    selectedTab = li;
    selectedTab.classList.add("active");
  }

  let dataTab = event.target.getAttribute("data-tab-name");

  for (let i = 0; i < alltabsContent.length; i++) {
    if (dataTab == i) {
      alltabsContent[i].classList.add("active-content");
    } else {
      alltabsContent[i].classList.remove("active-content");
    }
  }
};








    


