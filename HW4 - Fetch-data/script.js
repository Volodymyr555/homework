
  
function getFilms() {  
    
  return fetch("https://ajax.test-danit.com/api/swapi/films")
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      data.forEach((obj) => {
        const episodeId = obj.episodeId;
        const name = obj.name;
        const openingCrawl = obj.openingCrawl;
        const ul = document.querySelector("ul");

        ul.insertAdjacentHTML(
          "afterBegin",
          `<li id = '${episodeId}'>${episodeId} ${name} ${openingCrawl}</li>`
        );
        
        
      });
    });
    
          
};                           
              

function getPeople() {
  return fetch("https://ajax.test-danit.com/api/swapi/films")
    .then((response) => {
      return response.json();
    })

    .then((data) => {
      for (const el of data) {       
           const characters = el.characters;
        for (const item of characters) {       
               items = [item];          

            for (let i = 0; i < items.length; i++) {
              fetch(items[i])
                .then((response) => {
                  return response.json();
              } )
                .then((data) => {
                  const li = document.querySelectorAll('li');
                  let peopleId = el["id"];

                  li.forEach(elem => {
                    let liId = elem['id'];          
                    let p = document.createElement('p');                  
                    p.innerHTML = data.name;          
                        
                    if (liId == peopleId) {
                      elem.append(p);
                  }
        }
                    )    
              }
              )

          }                
          
        };                                     
                  
                    
      }
    });
}
Promise.all(getFilms(), getPeople());





