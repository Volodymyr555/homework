async function getUsers(){
    const response = await fetch('https://ajax.test-danit.com/api/json/users');
    const data = await response.json();
 return data

}

async function getPosts() {
  const response = await fetch("https://ajax.test-danit.com/api/json/posts");
  const data = await response.json();

    return data
}

Promise.all([getUsers(), getPosts()])
  .then((response) => {
    
    let arr1 = [];
    arr1 = response[0];
    let arr2 = [];
    arr2 = response[1];
    
    arr2.forEach((elem) => {
      
      let ul = document.querySelector('ul');
      let li = document.createElement('li');
      

      function getName() {
        for (const el of arr1) {
          if (elem.userId == el.id) {
            return el.name
          }
        }
      }

      function getEmail() {
        for (const el of arr1) {
          if (elem.userId == el.id) {
            return el.email
          }
        }

      };

      class User {
        constructor(title, text, name, email, id) {
          this.title = title
          this.text = text
          this.name = name
          this.email = email
          this.id = id
          
        }

      }

      let user = new User(elem.title, elem.body, getName(), getEmail(), elem.id);
      
      ul.insertAdjacentHTML(
        "beforeend",
        `<li id = "${user.id}"><h2>${user.name}</h2><h3>${user.title}</h3><p>${user.text}</p><div>${user.email}</div><button>delete</button></li>`
      );     
            
    })
  
  })
  .then((response) => {
    const btn = document.querySelectorAll("button");

    for (const i of btn) {
      i.addEventListener('click', (event) => {
        let liId = event.target.closest("li").id
         fetch(`https://ajax.test-danit.com/api/json/posts/${liId}`, {
           method: "DELETE",
         }).then((response) => {
           if (response.ok) {
           event.target.closest('li').remove()
         } });    
  
})
    }
   
  })






  