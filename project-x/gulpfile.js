const { src, dest, watch, parallel } = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const concat = require("gulp-concat");
const browserSync = require("browser-sync");

function styles() {
  return src("src/scss/style.scss")
    .pipe(sass({ outputStyle: "compressed" }))
    .pipe(concat("style.min.css"))
    .pipe(dest("src/css"))
      .pipe(browserSync.stream())      
      ;
}

function scripts() {
  return src(["src/js/app.js"])
    .pipe(concat("app.min.js"))
    .pipe(dest("src/js/js"))
    .pipe(browserSync.stream())
    ;
}



exports.styles = styles;
exports.scripts = scripts;

