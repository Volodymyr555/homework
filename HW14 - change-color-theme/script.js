const btn = document.createElement("button");
btn.innerHTML = "Змінити тему";
const body = document.querySelector('body');
const div = document.querySelector('.main');

body.append(btn);

function change() {
    body.classList.toggle("body-1");
    div.classList.toggle('main-1');
    localStorage.setItem('theme', '1');

    if (!body.classList.contains("body-1"))
    {
        localStorage.clear();
    }    
};

btn.onclick = change;

if (localStorage.getItem("theme") === "1")
{
    body.classList.add("body-1");
    div.classList.add("main-1");
};