class User {
    constructor(firstName, lastName, email, password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    setPassword(password) {
        const encryptedPassword = hashFunction(password);
        this.password = encryptedPassword;
    }
    checkPassword(password) {
        const encryptedPassword = hashFunction(password);
        this.password === encryptedPassword;
    }
    getFullName() {
        return this.firstName + ' ' + this.lastName;
    }
    getEmail() {
        return this.email;
    }
}

// function hashFunction(string) {
    
//     return crypto.createHash('sha256').update(string).digest('hex');
// }

function hashFunction(string) { return btoa(string)};

let user = new User('Volodymyr', 'Medvedchuk', 'meeedy@ukr.net', 'first')
console.log(user.setPassword('first'));
console.log(user.checkPassword('frst'));