class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  getName() {
    return this.name;
  }

  setName(value) {
    this.name = value;
  }

  getAge() {
    return this.age;
  }

  setAge(value) {
    this.age = value;
  }
  getSalary() {
    return this.salary;
  }

  setSalary(value) {
    this.slary = value;
  }
}


class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
      this.lang = lang;
      this.salary = salary * 3;
    };    
    
    
 
}

const ProgrammerOne = new Programmer('igor', 23, 6, "inglish")
const ProgrammerTwo = new Programmer('irina', 22, 5, 'china')

console.log({ProgrammerOne});

console.log({ProgrammerTwo});
