const div = document.querySelector('#root')
const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];


try {



for (let i = 0; i < books.length; i++)
{ 
  const ul = document.createElement('ul');
  
const author = books[i]['author'];
const name = books[i]['name'];
const price = books[i]['price'];

  ul.innerHTML = "<li>" + author + " " + name + " " + price + "</li>";

  if (!author || !name || !price) {
    throw new SyntaxError('значення відсутнє')
  } 
 
   div.append(ul);
}  
} catch (error) {
  for (let i = 1; i < books.length; i++) {
    const ul = document.createElement("ul");
    const author = books[i]["author"];
    const name = books[i]["name"];
    const price = books[i]["price"];

    ul.innerHTML = "<li>" + author + " " + name + " " + price + "</li>";
    

    div.append(ul);
  }  
  throw error 
  
};

    
 
